const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql');
var crypto = require('crypto');
const app = express();

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());


app.set('view engine', 'ejs');
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : 'qwerty',
  database : 'users'
});
connection.connect(function(err) {
  if (err) throw err;
  console.log('You are now connected...');
});
/*connection.query('create table accounts (id int(50) not null auto_increment primary key,name varchar(255),password varchar(255),username varchar(255));', function(err, result) {
  if (err) throw err;
  console.log(result);
});*/
app.get('/', (req, res) => {
  return res.render('hello', {action: "Hello World!"});
});

app.get('/users', (req, res) => {
  let result = connection.query('SELECT * FROM accounts', function(err, result){
    if(err) throw err;
    return res.json({result: result});
  });
});

app.post('/', (req, res) => {
      shasumPass = crypto.createHash('sha1');
      shasumPass.update(req.body.Password);
      passHash = shasumPass.digest('hex');
      connection.query('INSERT INTO accounts(name, password, username) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE name=?, password=?', [req.body.Name, passHash, req.body.Username, req.body.Name, passHash], function(err, result) {
        if (err) throw err;
        return res.json({name: req.body.Name, password: req.body.Password, username: req.body.Username });
      });
});

app.delete('/', (req, res) => {
  connection.query('DELETE FROM accounts WHERE id=?',[req.body.id], function(err, result){
    if(err) throw err;
  });
  return res.json({action: "User Deleted"});
});

app.listen(3001, () => console.log('Example app listening on port 3001!'));
